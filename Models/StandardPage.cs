using Piranha.AttributeBuilder;
using Piranha.Models;

namespace Zen_form.Models
{
    [PageType(Title = "Standard page")]
    public class StandardPage  : Page<StandardPage>
    {
    }
}