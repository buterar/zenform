﻿using Piranha.AttributeBuilder;
using Piranha.Extend;
using Piranha.Models;
using Piranha.Extend.Fields;
using System;

namespace Zen_form.Models
{
    [SiteType(Title = "Footer")]
    public class Footer : SiteContent<Footer>
    {
        public TextField TextFooter { get; set; }
    }
}
